import lib from '/js/lib.js'

const main = () => {
    const updateSelected = () => 
        lib.domSelect('footer > span').innerText = 
            Array.from(lib.domSelectAll('article'))
                .filter(article => lib.hasClass(article, 'selected'))
                .map(article => article.id)
                .toString()

    const clearSelection = () => {
        lib.domSelectAll('article').forEach(article => {
            lib.removeClass(article, 'selected')
            updateSelected()
        })
    }

    lib.domSelectAll('article').forEach(elem => {
        elem.onclick = event => {
            const multiselect = lib.domSelect('header input').checked

            let article = event.target
            while (article.tagName !== "ARTICLE")
                article = article.parentElement

            if (multiselect) {
                lib.toggleClass(article, 'selected')
            } else {
                const selected = lib.hasClass(article, 'selected')
                clearSelection()
                if (!selected) lib.addClass(article, 'selected')
            }
            
            event.stopPropagation();
            updateSelected()
        }
    })

    lib.domSelect('body').onclick = event => {
        clearSelection()
        event.stopPropagation();
    }

    lib.domSelect('header input').onchange = event => {
        clearSelection()
    }

    updateSelected()
}
 
export default main
console.log('Main module loaded.')

