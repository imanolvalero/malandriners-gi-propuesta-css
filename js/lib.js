const lib = {
	hasClass: (elem, className) => {
		return new RegExp(" " + className + " ").test(
			" " + elem.className + " "
		);
	},
	addClass: (elem, className) => {
		if (!lib.hasClass(elem, className)) {
			elem.className += " " + className;
		}
	},
	removeClass: (elem, className) => {
		var newClass = " " + elem.className.replace(/[\t\r\n]/g, " ") + " ";
		if (lib.hasClass(elem, className)) {
			while (newClass.indexOf(" " + className + " ") >= 0) {
				newClass = newClass.replace(" " + className + " ", " ");
			}
			elem.className = newClass.replace(/^\s+|\s+$/g, "");
		}
	},
	toggleClass: (elem, className) => {
		const funcs = {
			false: lib.addClass,
			true: lib.removeClass,
		};
		funcs[lib.hasClass(elem, className)](elem, className);
	},
	domSelect: (selectString) => {
		return document.querySelector(selectString);
	},
	domSelectAll: (selectString) => {
		return document.querySelectorAll(selectString);
	},
};

export default lib;
let module = { exports: { lib } };

console.log("Lib module loaded.");
