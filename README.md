# Malandriners
## Grupo Intensivo
### Propuesta de mejora de CSS

Es una pequeña aportación al _side-proyect_ de [Dani Pastor](https://www.danielprimo.io/#comunidad), para que elimine el boton [Borrar selección]

Espero que te aporte algo Dani!

[[Ver en vivo]](https://imanolvalero.gitlab.io/malandriners-gi-propuesta-css)